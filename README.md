# MyDockerImages

Features
This is the list of all features supported by the current version of drill:

Multi thread: run your benchmarks setting as many concurrent threads as you want.
Multi iterations: specify the number of iterations you want to run the benchmark.
Ramp-up: specify the amount of time it will take drill to start all threads.
Delay: introduce controlled delay between requests. Example: assigns.yml
Dynamic urls: execute requests with dynamic interpolations in the url, like /api/users/{{ item }}
Dynamic headers: execute requests with dynamic headers. Example: headers.yml
Interpolate environment variables: set environment variables, like /api/users/{{ EDITOR }}
Request dependencies: create dependencies between requests with assign and url interpolations.
Split files: organize your benchmarks in multiple files and include them.
CSV support: read CSV files and build N requests fill dynamic interpolations with CSV data.
HTTP methods: build request with different http methods like GET, POST, PUT, PATCH, HEAD or DELETE.
Cookie support: create benchmarks with sessions because cookies are propagates between requests.
Stats: get nice statistics about all the requests. Example: cookies.yml
Thresholds: compare the current benchmark performance against a stored one session and fail if a threshold is exceeded.